---
layout: handbook-page-toc
title: "Enablement:Gitaly Team"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## What is Gitaly?

The Gitaly team is responsible for building and maintaining systems to ensure
that the Git data storage tier of GitLab instances, and _GitLab.com in particular_,
is reliable, secure and fast. For more information about Gitaly, see our [Direction](https://about.gitlab.com/direction/gitaly/) page and [roadmap](https://gitlab.com/groups/gitlab-org/-/epics/7608).

Gitaly team consists of two subgroups, [Gitaly:Cluster team](#cluster-team) and [Gitaly:Git team](#git-team), together referred to as **Gitaly**.

### Functional boundary

While GitLab is the primary consumer of the Gitaly project, Gitaly is a standalone product which can be used external to GitLab. As such, we strive to achieve a functional boundary around Gitaly. The goal of this is to ensure that the Gitaly project creates an interface to manage Git data, but does not make business decisions around how to manage the data.

For example, Gitaly can provide a robust and efficient set of APIs to move Git repositories between storage solutions, but it would be up to the calling application to decide when such moves should occur.

Processes fully independent of business inputs (such as repository maintenance) should be fully contained within Gitaly as they provide substantial value to anyone using the Gitaly project.

## Stable Counterparts

The following members of other functional teams are our stable counterparts:

<%= stable_counterparts(role_regexp: /[&,] Systems:Gitaly( API)?/, direct_manager_role: 'Backend Engineering Manager, Gitaly') %>

## How to contact the team

### Urgent issues and outages

Gitaly team members do not carry pagers, but we live around the world and there's a good chance that someone is available during their working hours. There is no coverage for weekends; instead, we strive to empower incident responders to mitigate any circumstance.

These issues relate to ongoing production outages or similar. They interrupt our process used to [schedule work] and get attention as soon as possible.
Please only interrupt us sparingly, in these cases:
- [Severity 1 or 2](https://about.gitlab.com/handbook/engineering/quality/issue-triage/#availability) issues, where you believe that Gitaly team has _immediately actionable_ work to do
- Ongoing incidents where no mitigation exists that would tide us over until the next business day.

**Getting attention on an urgent, interrupting issue**

- If there's no issue filed yet, file one in the [Gitaly issue tracker] (remember that Security incidents and those containing customer details should be Confidential).
- Tag Engineering Manager and Product manager (listed above) as well as `@gl-gitaly` (the [whole team](https://gitlab.com/groups/gl-gitaly/-/group_members)) on the issue.
- Post on [#g_gitaly] on Slack, mention the issue, and tag EM and PM again.
- The issue will be attended to by the first available staff member during their working hours, who will assign it to themselves, and explicitly hand it off by reassigning to the next person should this be necessary.

### Customer issues

Please file an issue [here](https://gitlab.com/gitlab-org/gitaly/-/issues/new?issuable_template=Support%20Request). Post it on [#g_gitaly] for more immediate visibility.

### Normal priority requests

We use the standard [engineering workflow] to [schedule work]. To get Gitaly team
work on something, it's best to create an issue on the [Gitaly issue tracker]
and add the `~"group::gitaly"` and `~"workflow::problem validation"` labels,
along with any other appropriate labels.  Then, feel free to tag the relevant
Product Manager and/or Engineering Manager as listed above.

For information requests and other quick one-offs, feel free to use [#g_gitaly] on Slack to get attention on the issue.

[schedule work]: https://gitlab.com/gitlab-org/gitaly/-/issues/4095
[engineering workflow]: https://about.gitlab.com/handbook/product-development-flow/#workflow-summary
[Gitaly issue tracker]: https://gitlab.com/gitlab-org/gitaly/issues
[Gitaly ongoing work issue board]: https://gitlab.com/groups/gitlab-org/-/boards/1140874?label_name%5B%5D=group%3A%3Agitaly&milestone_title=Upcoming
[#g_gitaly]: https://gitlab.slack.com/archives/g_gitaly

### Issues with `Infradev` labels

These are typically [Corrective Actions or other followup items](https://about.gitlab.com/handbook/engineering/workflow/#infradev) that have strict
SLO tracking. They will be scheduled through either of the above paths, by EM
and/or PM polling these dashboards:


## Cluster team

**Mission**: Provide a durable, performant, and reliable Git storage layer for GitLab.

[Responsibilities](https://about.gitlab.com/job-families/engineering/backend-engineer/#cluster) |
[In the product hierarchy](https://about.gitlab.com/handbook/product/categories/#gitalycluster-group)

<%= department_team(base_department: 'Gitaly Cluster Team') %>

## Git team

**Mission**: Develop Git in accordance with the goals of the community and GitLab, and integrate it into our products.

[Responsibilities](https://about.gitlab.com/job-families/engineering/backend-engineer/#git) |
[In the product hierarchy](https://about.gitlab.com/handbook/product/categories/#gitalygit-group)

<%= department_team(base_department: 'Gitaly Git Team') %>

## Working with product

A weekly call is held between the product manager and engineering manager,
which is listed in the "Gitaly Team" calendar. Everyone is welcome to join and
these calls are used to discuss any roadblocks, concerns, status updates,
deliverables, or other thoughts that impact the group.

### Gitaly consumers

To have a constant communication flow about planned changes, updates and maybe
breaking changes we have the [#g_gitaly] Slack channel. In the
channel we will provide updates for all teams using the service but also ask
for assistance to provide feedback and insights about planned changes or improvements.

To support this pro-active communication additionally there is also an individual
counterpart on the consumer side to help with research in the codebases and
coordination with all the teams consuming Gitaly. The DRI on Consumer side is Igor Drozdov.

The Gitaly consumers are:

* [GitLab Rails](https://gitlab.com/gitlab-org/gitlab)
* [GitLab Shell](https://gitlab.com/gitlab-org/gitlab-shell)
* [GitLab Workhorse](https://gitlab.com/gitlab-org/gitlab/-/blob/master/doc/development/workhorse/index.md)
* [GitLab Elasticsearch Indexer](https://gitlab.com/gitlab-org/gitlab-elasticsearch-indexer)

## Metrics

### Useful links

* [Actual pending Infradev issues](https://app.periscopedata.com/app/gitlab/899982/InfraDev?widget=12327718&udv=0) (sort by group, focus on gitaly)
- [Out of SLO Infradev issues](https://gitlab.okta.com/app/periscopedata/exk3fzi3sc0TkcYSj357/sso/saml)
* [Error budget](https://dashboards.gitlab.net/d/stage-groups-detail-gitaly/stage-groups-gitaly-group-error-budget-detail?orgId=1)
- [MR review workload](https://gitlab-org.gitlab.io/gitlab-roulette/?currentProject=gitaly)

<%= partial "handbook/engineering/metrics/partials/_cross_functional_dashboard.erb", locals: { filter_value: "Gitaly" } %>

## Team development

### Onboarding

To complete team-specific onboarding, please file an issue
[here](https://gitlab.com/gitlab-org/gitaly/-/issues/new?issuable_template=Team%20Member%20Onboarding).

### Offboarding

Maintainer rights are revoked, and to remove the developer from the list of
authorized approvers, remove them from the `gl-gitaly` GitLab.com group.
